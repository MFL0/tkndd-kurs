# TKNDD - Kurs

Kursy wiedzy TKNDD

## Instalacja

zainstaluj [pip](https://pip.pypa.io/en/stable/) by móc pobrać rozszerzenia konieczne do uruchomienia programu

##### Windows:
```zsh
pip install -r requirements.txt 
```

##### macOS/Linux:
```zsh
pip3 install -r requirements.txt
```

## Uruchamianie

##### Windows:
```zsh
python app.py
```
##### macOS/Linux:
```zsh
python3 app.py
```